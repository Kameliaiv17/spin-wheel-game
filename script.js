const SECTORS = 12;
const WHEEL = document.querySelector('.wheel');
const MESSAGE = document.querySelector('.message');
const SPIN_BUTTON = document.querySelector('.spinBtn');

let spinsLeft = 10000000;
let freeSpinsLeft = 0;
let freeSpinWins = 0;
let balance = 100;
let costPerSpin = 10;
let value = Math.ceil(Math.random() * 3600);
let lastTwo = [null, null];
let targetSector1 = 0;
let targetSector2 = 1;
let targetCount1 = 0;
let targetCount2 = 0;

const SECTOR_REWARDS = {
  0: { reward: 100, label: "100 points" },
  1: { reward: 20, label: "20 points" },
  2: { reward: 0, label: "Lose" },
  3: { reward: 60, label: "60 points" },
  4: { reward: 70, label: "70 points" },
  5: { reward: 30, label: "30 points" },
  6: { reward: 0, label: "Free Spins" },
  7: { reward: 0, label: "Lose" },
  8: { reward: 100, label: "100 points" },
  9: { reward: 50, label: "50 points" },
  10: { reward: 40, label: "40 points" },
  11: { reward: 0, label: "Lose" }
};

const HALF_SECTOR = 180 / SECTORS;

function getNextSector() {
  let sector;
  do {
    sector = Math.floor(Math.random() * SECTORS);
    if (sector == targetSector1 && targetCount1 < 3 && lastTwo[0] != targetSector1 && lastTwo[1] != targetSector1) {
      targetCount1++;
      break;
    } else if (sector == targetSector2 && targetCount2 < 2 && lastTwo[0] != targetSector2 && lastTwo[1] != targetSector2) {
      targetCount2++;
      break;
    }
  } while (sector == lastTwo[0] || sector == lastTwo[1]);

  lastTwo[0] = lastTwo[1];
  lastTwo[1] = sector;

  return sector;
}

function spinWheel() {
  if ((spinsLeft <= 0 && freeSpinsLeft <= 0) || balance < 10) {
    SPIN_BUTTON.disabled = true;
    if (balance < 10) {
      MESSAGE.textContent = 'Game over!';
    }
    return;
  }

  if (freeSpinsLeft > 0) {
    freeSpinsLeft--;
  } else {
    balance -= costPerSpin;
    spinsLeft--;
  }

  value += getNextSector() * (360 / SECTORS) + 3600;
  WHEEL.style.transition = 'transform 5s ease-out';
  WHEEL.style.transform = `rotate(${value}deg)`;
}

WHEEL.addEventListener('transitionend', () => {
  const stoppedAt = (360 - (value % 360) + HALF_SECTOR) % 360;
  const sectorIndex = Math.round(stoppedAt / (360 / SECTORS)) % SECTORS;
  const reward = SECTOR_REWARDS[sectorIndex].reward;

  if (reward === 0 && SECTOR_REWARDS[sectorIndex].label === "Free Spins") {
    freeSpinsLeft += 3;
    MESSAGE.textContent = 'Free Spins! You get 3 extra spins!';
  } else {
    balance += reward;
    if (freeSpinsLeft > 0) {
      freeSpinWins += reward;
    }
    MESSAGE.textContent = `You won ${reward}$ ! Your balance is now ${balance}$ .`;
  }

  WHEEL.style.transition = 'none';

  if (freeSpinsLeft > 0) {
    setTimeout(() => {
      WHEEL.style.transition = 'transform 5s ease-out';
      spinWheel();
    }, 1000);
  } else if (freeSpinWins > 0) {
    MESSAGE.textContent = `You won a total of ${freeSpinWins}$ from Free Spins! Your balance is now ${balance}$.`;
    freeSpinWins = 0;
  }
});

SPIN_BUTTON.addEventListener('click', spinWheel);